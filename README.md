# SkyhookDM Query Optimizer

This repository contains code for a parser to read and translate queries into query plans and an
optimizer to rewrite the query plan as appropriate.

The goal is allow a plan created and propagated from the SkyhookDM client to be rewritten
individually at an OSD or CSD (computational storage device).
